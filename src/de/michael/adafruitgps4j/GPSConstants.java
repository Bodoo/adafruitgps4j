package de.michael.adafruitgps4j;

public class GPSConstants {

    // Data output echo rate commands
    public static final String PMTK_SET_NMEA_UPDATE_100_MILLIHERTZ  = "$PMTK220,10000*2F";
    public static final String PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ  = "$PMTK220,5000*1B";
    public static final String PMTK_SET_NMEA_UPDATE_1HZ             = "$PMTK220,1000*1F";           // [Default]
    public static final String PMTK_SET_NMEA_UPDATE_5HZ             = "$PMTK220,200*2C";
    public static final String PMTK_SET_NMEA_UPDATE_10HZ            = "$PMTK220,100*2F";

    // Position fix update rate commands (can't fix position faster than 5 times a second)
    public static final String PMTK_API_SET_FIX_CTL_100_MILLIHERTZ  = "$PMTK300,10000,0,0,0,0*2C";  // Once every 10 seconds, 100 millihertz.
    public static final String PMTK_API_SET_FIX_CTL_200_MILLIHERTZ  = "$PMTK300,5000,0,0,0,0*18";   // Once every 5 seconds, 200 millihertz.
    public static final String PMTK_API_SET_FIX_CTL_1HZ             = "$PMTK300,1000,0,0,0,0*1C";   // Once every second, 1 hertz. [Default]
    public static final String PMTK_API_SET_FIX_CTL_5HZ             = "$PMTK300,200,0,0,0,0*2F";    // 5 times every second, 5 hertz.
    
    // GPS Baudrate commands 
    public static final String PMTK_SET_BAUD_115200                 = "$PMTK251,115200*1F";
    public static final String PMTK_SET_BAUD_57600                  = "$PMTK251,57600*2C";          
    public static final String PMTK_SET_BAUD_9600                   = "$PMTK251,9600*17";           // [Default]
    
    // Sentence output configuration
    public static final String PMTK_SET_NMEA_OUTPUT_RMCONLY         = "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29";  // turn on only the second sentence (GPRMC)
    public static final String PMTK_SET_NMEA_OUTPUT_RMCGGA          = "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28";  // turn on GPRMC and GGA
    public static final String PMTK_SET_NMEA_OUTPUT_ALLDATA         = "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28";  // turn on ALL THE DATA
    public static final String PMTK_SET_NMEA_OUTPUT_OFF             = "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28";  // turn off output
    
    // to generate your own sentences, check out the MTK command datasheet and use a checksum calculator
    // such as the awesome http://www.hhhh.org/wiml/proj/nmeaxor.html
    
    
    public static final String PMTK_LOCUS_STARTLOG                  = "$PMTK185,0*22";
    public static final String PMTK_LOCUS_STOPLOG                   = "$PMTK185,1*23";
    public static final String PMTK_LOCUS_STARTSTOPACK              = "$PMTK001,185,3*3C";
    public static final String PMTK_LOCUS_QUERY_STATUS              = "$PMTK183*38";
    public static final String PMTK_LOCUS_ERASE_FLASH               = "$PMTK184,1*22";
    public static final int    LOCUS_OVERLAP                        = 0;
    public static final int    LOCUS_FULLSTOP                       = 1;

    public static final String PMTK_ENABLE_SBAS                     = "$PMTK313,1*2E";
    public static final String PMTK_ENABLE_WAAS                     = "$PMTK301,2*2E";
    
    // standby command & boot successful message
    public static final String PMTK_STANDBY                         = "$PMTK161,0*28";
    public static final String PMTK_STANDBY_SUCCESS                 = "$PMTK001,161,3*36";          // Not needed currently
    public static final String PMTK_AWAKE                           = "$PMTK010,002*2D";
    
    // ask for the release and version
    public static final String PMTK_Q_RELEASE                       = "$PMTK605*31";
    
    // request for updates on antenna status 
    public static final String PGCMD_ANTENNA                        = "$PGCMD,33,1*6C";
    public static final String PGCMD_NOANTENNA                      = "$PGCMD,33,0*6D";
}
