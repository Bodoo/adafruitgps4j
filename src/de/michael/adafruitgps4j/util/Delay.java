package de.michael.adafruitgps4j.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Delay {
    
    public static void delay(int millies){
        try {
            Thread.sleep(millies);
        } catch (InterruptedException ex) {
            Logger.getLogger(Delay.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
