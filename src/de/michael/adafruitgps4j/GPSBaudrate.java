package de.michael.adafruitgps4j;

public enum GPSBaudrate {
    
    _9600(9600),
    _57600(57600),
    _115200(115200);
    
    private int bitsPerSecond;

    private GPSBaudrate(int bitsPerSecond) {
        this.bitsPerSecond = bitsPerSecond;
    }
    
    public int bitsPerSecond(){
        return bitsPerSecond;
    }
}
