package de.michael.adafruitgps4j.serialimpl;

import de.michael.adafruitgps4j.IGPSEventListener;
import de.michael.adafruitgps4j.NMEAType;
import java.util.HashMap;

public class SerialDataReceivedHandler implements SerialDataReceiver {

    private IGPSEventListener igpsevent;
    private HashMap<NMEAType, String> nmeaCache;
    private NMEAType lastNMEAType = NMEAType.UNKNOWN;

    public SerialDataReceivedHandler(IGPSEventListener igpsevent) {
        this.igpsevent = igpsevent;
        nmeaCache = new HashMap<>();
    }

    @Override
    public void onSerialDataReceiveASCII(String asciidata) {
        int lastindex = asciidata.lastIndexOf("$");
        if (lastindex != -1 && lastindex < asciidata.length() - 6) {
            lastNMEAType = NMEAType.byPrefix(asciidata.substring(lastindex, lastindex + 6));
        }
        String[] fulldata = asciidata.split("\\$");
        for (String tile : fulldata) {
            if (tile.length() > 5) {
                tile = "$" + tile;
                if (tile.toLowerCase().contains("gpgga")) {
                    nmeaCache.put(NMEAType.GPGGA, tile);
                    igpsevent.onReceive(NMEAType.GPGGA, tile);
                } else if (tile.toLowerCase().contains("gpgll")) {
                    nmeaCache.put(NMEAType.GPGLL, tile);
                    igpsevent.onReceive(NMEAType.GPGLL, tile);
                } else if (tile.toLowerCase().contains("gpgsa")) {
                    nmeaCache.put(NMEAType.GPGSA, tile);
                    igpsevent.onReceive(NMEAType.GPGSA, tile);
                } else if (tile.toLowerCase().contains("gpgsv")) {
                    nmeaCache.put(NMEAType.GPGSV, tile);
                    igpsevent.onReceive(NMEAType.GPGSV, tile);
                } else if (tile.toLowerCase().contains("gprmc")) {
                    nmeaCache.put(NMEAType.GPRMC, tile);
                    igpsevent.onReceive(NMEAType.GPRMC, tile);
                } else if (tile.toLowerCase().contains("gpvtg")) {
                    nmeaCache.put(NMEAType.GPVTG, tile);
                    igpsevent.onReceive(NMEAType.GPVTG, tile);
                } else {
                    System.out.println("[SerialDataReceivedHandler]: Received unknown type data: '" + asciidata + "'");
                }
            }
        }
    }

    public String getLastNMEAData(NMEAType type) {
        return nmeaCache.get(type);
    }

    @Override
    public NMEAType getLastNMEAType() {
        return lastNMEAType;
    }

}
