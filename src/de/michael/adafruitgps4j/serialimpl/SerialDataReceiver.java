package de.michael.adafruitgps4j.serialimpl;

import de.michael.adafruitgps4j.NMEAType;

public interface SerialDataReceiver {
    
    void onSerialDataReceiveASCII(String asciidata);
    
    NMEAType getLastNMEAType();
    
}
