package de.michael.adafruitgps4j.serialimpl;

import java.io.IOException;

public interface ISerialImplementation {
    
    void open(String device, int baudrate, SerialDataReceiver serialDataReceivedEvent) throws IOException;
    
    void close() throws IOException;
    
    void write(String data) throws IOException;
    
    boolean isPi4J();
    default boolean isDirect() { return !isPi4J(); }
}
