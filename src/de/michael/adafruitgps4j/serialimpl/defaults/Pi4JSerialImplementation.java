package de.michael.adafruitgps4j.serialimpl.defaults;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import de.michael.adafruitgps4j.serialimpl.ISerialImplementation;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import de.michael.adafruitgps4j.serialimpl.SerialDataReceiver;

public class Pi4JSerialImplementation implements ISerialImplementation {

    private Serial serial;

    @Override
    public void open(String device, int baudrate, SerialDataReceiver serialDataReceivedEvent) throws IOException {
        serial = SerialFactory.createInstance();
        SerialConfig config = new SerialConfig();
        config.device(device)
                .baud(Baud.valueOf("_"+baudrate))
                .dataBits(DataBits._8)
                .parity(Parity.NONE)
                .flowControl(FlowControl.NONE);
        
        //add listener
        serial.addListener((e)->{
            try {
                serialDataReceivedEvent.onSerialDataReceiveASCII(e.getAsciiString().replaceAll("\r\n", ""));
            } catch (IOException ex) {
                Logger.getLogger(Pi4JSerialImplementation.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        serial.open(config);
        System.out.println("Opening device '"+device+"' with baud="+baudrate);
    }

    @Override
    public void close() throws IOException {
        if(serial != null){
            serial.discardAll();
            serial.close();
            serial = null;
        }
        System.out.println("closing device");
    }

    @Override
    public void write(String data) throws IOException{
        serial.write(data);
        System.out.println("writing data to device :'"+data+"'");
    }

    @Override
    public boolean isPi4J() {
        return true;
    }

}
