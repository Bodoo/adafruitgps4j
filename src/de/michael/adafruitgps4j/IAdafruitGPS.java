package de.michael.adafruitgps4j;

import de.michael.adafruitgps4j.serialimpl.SerialDataReceiver;
import de.michael.adafruitgps4j.serialimpl.SerialImplType;
import java.io.IOException;

public interface IAdafruitGPS {
    
    void begin(int baud, SerialImplType implementationType, IGPSEventListener igpsevent) throws IOException;
    
    void changeBaudrate(GPSBaudrate gpsBaudrate) throws IOException;
    
    void sendCommand(String command);
    
    void setOutputType(GPSOutputType gpsOutputType);
    
    String getLastNMEAData(NMEAType type);
    
    SerialDataReceiver getSerialDataReceiver();
    
    boolean wakeup();
    boolean standby();
    
    boolean isAwake();
    
    boolean waitForSentence(String wait);
    
}
