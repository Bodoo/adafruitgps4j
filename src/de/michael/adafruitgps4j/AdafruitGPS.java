package de.michael.adafruitgps4j;

import de.michael.adafruitgps4j.serialimpl.ISerialImplementation;
import de.michael.adafruitgps4j.serialimpl.SerialImplType;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static de.michael.adafruitgps4j.GPSConstants.*;
import de.michael.adafruitgps4j.serialimpl.SerialDataReceivedHandler;
import de.michael.adafruitgps4j.serialimpl.SerialDataReceiver;
import de.michael.adafruitgps4j.serialimpl.defaults.DirectSerialImplementation;
import de.michael.adafruitgps4j.serialimpl.defaults.Pi4JSerialImplementation;
import de.michael.adafruitgps4j.util.Delay;

public class AdafruitGPS implements IAdafruitGPS {

    private ISerialImplementation serialImpl;
    private String device;
    private IGPSEventListener gpsEventListener;
    private SerialDataReceivedHandler serialDataReceivedHandler;
    private volatile boolean inStandbyMode;

    int hour, minute, seconds, year, month, day;
    int milliseconds;
    // Floating point latitude and longitude value in degrees.
    float latitude, longitude;
    // Fixed point latitude and longitude value with degrees stored in units of 1/100000 degrees,
    // and minutes stored in units of 1/100000 degrees.  See pull #13 for more details:
    //   https://github.com/adafruit/Adafruit-GPS-Library/pull/13
    int latitude_fixed, longitude_fixed;
    float latitudeDegrees, longitudeDegrees;
    float geoidheight, altitude;
    float speed, angle, magvariation, HDOP;
    char lat, lon, mag;
    boolean fix;
    int fixquality, satellites;

    public AdafruitGPS(String device) {
        this.device = device;
        common_init();
    }

    @Override
    public void begin(int baud, SerialImplType implementationType, IGPSEventListener igpsevent) throws IOException {
        switch (implementationType) {
            case PI4J:
                serialImpl = new Pi4JSerialImplementation();
                break;
            case DIRECT:
                serialImpl = new DirectSerialImplementation();
        }
        this.gpsEventListener = igpsevent;
        serialDataReceivedHandler = new SerialDataReceivedHandler(igpsevent);
        serialImpl.open(device, baud, serialDataReceivedHandler);
    }

    private void common_init() {
        hour = minute = seconds = year = month = day = fixquality = satellites = 0; // uint8_t
        lat = lon = mag = 0; // char
        fix = false; // boolean
        milliseconds = 0; // uint16_t
        latitude = longitude = geoidheight = altitude = speed = angle = magvariation = HDOP = 0.0f; // float
        inStandbyMode = false;
    }

    @Override
    public void sendCommand(String command) {
        try {
            serialImpl.write(command + "\r\n");
        } catch (IOException ex) {
            Logger.getLogger(AdafruitGPS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean wakeup() {
        if (inStandbyMode) {
            inStandbyMode = false;
            sendCommand("");  // send byte to wake it up
            return waitForSentence(PMTK_AWAKE);
        } else {
            return false;  // Returns false if not in standby mode, nothing to wakeup
        }
    }

    @Override
    public boolean standby() {
        if (inStandbyMode) {
            return false;  // Returns false if already in standby mode, so that you do not wake it up by sending commands to GPS
        } else {
            inStandbyMode = true;
            sendCommand(PMTK_STANDBY);
            //return waitForSentence(PMTK_STANDBY_SUCCESS);  // don't seem to be fast enough to catch the message, or something else just is not working
            return true;
        }
    }

    @Override
    public boolean waitForSentence(String wait) {

        return false;
    }

    @Override
    public void changeBaudrate(GPSBaudrate gpsBaudrate) throws IOException {
        switch (gpsBaudrate) {
            case _9600:
                sendCommand(PMTK_SET_BAUD_9600);
                break;
            case _57600:
                sendCommand(PMTK_SET_BAUD_57600);
                break;
            case _115200:
                sendCommand(PMTK_SET_BAUD_115200);
                break;
        }
        Delay.delay(200);
        serialImpl.close();
        serialImpl.open(device, gpsBaudrate.bitsPerSecond(), serialDataReceivedHandler);
    }

    @Override
    public boolean isAwake() {
        return !inStandbyMode;
    }

    @Override
    public void setOutputType(GPSOutputType gpsOutputType) {
        sendCommand(gpsOutputType.getCommand());
    }

    @Override
    public String getLastNMEAData(NMEAType type) {
        return serialDataReceivedHandler.getLastNMEAData(type);
    }

    @Override
    public SerialDataReceiver getSerialDataReceiver() {
        return serialDataReceivedHandler;
    }

}
