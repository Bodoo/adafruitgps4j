package de.michael.adafruitgps4j;

public enum GPSOutputType {
    
    RMCONLY(GPSConstants.PMTK_SET_NMEA_OUTPUT_RMCONLY),
    RMCGGA(GPSConstants.PMTK_SET_NMEA_OUTPUT_RMCGGA),
    ALLDATA(GPSConstants.PMTK_SET_NMEA_OUTPUT_ALLDATA),
    OFF(GPSConstants.PMTK_SET_NMEA_OUTPUT_OFF);
    
    private String command;

    private GPSOutputType(String command) {
        this.command = command;
    }
    
    public String getCommand(){
        return command;
    }
    
}
