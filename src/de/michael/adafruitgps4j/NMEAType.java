package de.michael.adafruitgps4j;

public enum NMEAType {
    
    GPGGA,
    GPGLL,
    GPGSA,
    GPGSV,
    GPRMC,
    GPVTG
    
    ,
    UNKNOWN;
    
    public static NMEAType byPrefix(String prefix){
        try{
            return valueOf(prefix.substring(1).toUpperCase());
        }catch(Exception ex){
            return UNKNOWN;
        }
    }
}
