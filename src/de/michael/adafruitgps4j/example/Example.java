package de.michael.adafruitgps4j.example;

import de.michael.adafruitgps4j.AdafruitGPS;
import de.michael.adafruitgps4j.GPSBaudrate;
import de.michael.adafruitgps4j.GPSOutputType;
import java.io.IOException;
import java.util.Scanner;

import de.michael.adafruitgps4j.serialimpl.SerialImplType;
import java.text.ParseException;

public class Example {

    public static AdafruitGPS adafruitGPS;
    
    public static void main(String[] args) throws InterruptedException, IllegalStateException, IOException, ParseException {
        try {
            adafruitGPS = new AdafruitGPS("/dev/ttyS0");
            adafruitGPS.begin(115200, SerialImplType.PI4J, new ExampleGPSParser());
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        
        Scanner scan = new Scanner(System.in);
        while (true) {
            String data = scan.nextLine();
            if (data.startsWith("0")) {
                adafruitGPS.changeBaudrate(GPSBaudrate._115200);
            } else if (data.startsWith("a")) {
                adafruitGPS.changeBaudrate(GPSBaudrate._9600);
            } else if (data.startsWith("b")) {
                adafruitGPS.changeBaudrate(GPSBaudrate._57600);
            } else if (data.startsWith("c")) {
                adafruitGPS.setOutputType(GPSOutputType.RMCONLY);
            } else if (data.startsWith("d")) {
                adafruitGPS.setOutputType(GPSOutputType.RMCGGA);
            } else if (data.startsWith("e")) {
                adafruitGPS.setOutputType(GPSOutputType.ALLDATA);
            } else if (data.startsWith("f")) {
                adafruitGPS.setOutputType(GPSOutputType.OFF);
            } else {
                adafruitGPS.sendCommand(data);
            }
            System.out.println("data: " + data);
        }

    }

}
