package de.michael.adafruitgps4j.example;

import de.michael.adafruitgps4j.IGPSEventListener;
import de.michael.adafruitgps4j.NMEAType;

public class ExampleGPSPrinter implements IGPSEventListener {

    @Override
    public void onReceive(NMEAType type, String data) {
        System.out.println("[GPS Printer] "+type.name().toUpperCase() + ": " + data);
        
    }

    
}
