package de.michael.adafruitgps4j.example;

import de.michael.adafruitgps4j.IGPSEventListener;
import de.michael.adafruitgps4j.NMEAType;
import de.michael.adafruitgps4j.util.NMEA;

public class ExampleGPSParser implements IGPSEventListener {

    private NMEA nmea;
    
    public ExampleGPSParser(){
        nmea = new NMEA();
    }
    
    @Override
    public void onReceive(NMEAType type, String data) {
        NMEA.GPSPosition p = nmea.parse(data);
        if(type.equals(Example.adafruitGPS.getSerialDataReceiver().getLastNMEAType())){
            System.out.println("GPSPosition: " + p.toString());
        }
    }
    
    
    
}
