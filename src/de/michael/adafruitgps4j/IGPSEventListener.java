package de.michael.adafruitgps4j;

public interface IGPSEventListener {
    
    public void onReceive(NMEAType type, String data);
    
}
